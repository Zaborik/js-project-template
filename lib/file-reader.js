import fs from 'fs'
import readline from 'readline'

/**
 * Wrapper to read files
 */
class FileReader {
    constructor(fileName) {
        this.fileName = fileName
    }

    /**
   * Reads the file line by line and invokes 
   * onRead for ever line
   * @param  {func} onRead is a callback function
   */
    readLineByLine(onRead){
        const fileStream = fs.createReadStream(this.fileName)
        const rl = readline.createInterface({
            input: fileStream
        })
        rl.on('line', onRead)
    }

    /**
   * Reads the file and parses it as JSON
   * @return {Promise}
   */
    readJSON(){
        return new Promise((resolve, reject) => {
            fs.readFile(this.fileName, (err, data)=>{
                if(err){
                    reject(err)
                } else {
                    resolve(JSON.parse(data))
                }
            })
        })
    }
}

export default FileReader