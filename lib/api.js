import axios from 'axios'
import http from 'http'
import https from 'https'

/**
 * API wrapper to make HTTP requests
 */
class API {
    constructor({ host }) {
        this.host = host
        this.client = axios.create({
            baseURL: this.host,
            // timeout: 1000,
            httpAgent: new http.Agent({ keepAlive: true }),
            httpsAgent: new https.Agent({ keepAlive: true }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
    }

    /**
   * Makes basic GET HTTP request
   * @param  {string} addr is the endpoint to hit
   * @return {Promise}
   */
    get(addr){
        return this.client.get(addr)
    }

    /**
   * makes basic POST HTTP request
   * @param  {string} addr is the endpoint to hit
   * @param  {Object} data is the body of the request
   * @return {Promise}
   */
    post(addr, data){
        return this.client.post(addr, data)
    }
}

export default API