import fs from 'fs'

/**
 * Wrapper to write to files
 */
class FileWriter {
    /**
   * FileWriter constructor
   * @param  {string} fileName where to store the file
   * @param  {bool} append indicates if data should be appended to EOF
   */
    constructor(fileName, append) {
        this.fileName = fileName
        this.open = new Promise((resolve) => {
            let options = {}
            if (append) {
                options = {
                    flags: 'a'
                }
            }
            this.writeStream = fs.createWriteStream(fileName, options)
            this.writeStream.on('open', () => {
                resolve()
            })
        })
    }

    /**
   * Writes data to the open stream
   * @param  {Buffer} data
   * @return {Promise}
   */
    write(data) {
        return this.open.then(() => {
            this.writeStream.write(data)
        })
    }

    /**
   * Writes an obnject in JSON format
   * @param  {Object} obj object to store
   * @return {Promise}
   */
    writeJSON(obj) {
        const buffer = Buffer.from(JSON.stringify(obj))
        return this.write(buffer)
    }

    /**
   * Closes the stream
   */
    close() {
        this.writeStream.close()
    }
}

export default FileWriter