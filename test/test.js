import nock from 'nock'
import API from '../lib/api'
import expect from 'unexpected' // http://unexpected.js.org/assertions/any/to-be/

describe('API', function() {
    let client
    let scope

    before('Set up API client', function(){
        client = new API({
            host: 'https://api.github.com'
        })
    })

    describe('#get()', function() {
        describe('when 200 http status', function() {
            before('Set up mock server response', function(){
                scope = nock('https://api.github.com')
                    .get('/repos/atom/atom/license')
                    .reply(200, {
                        license: {
                            key: 'mit',
                            name: 'MIT License',
                            spdx_id: 'MIT',
                            url: 'https://api.github.com/licenses/mit',
                            node_id: 'MDc6TGljZW5zZTEz',
                        },
                    })
                    .persist()
            })

            it('should return a valid response', async function() {
                const res = await client.get('/repos/atom/atom/license')
                expect(res.status, 'to equal', 200)
            })

            after('After example', function(){
                scope.persist(false)
            })
        })
    })

    describe('Error example', function(){
        it('throws an error', function(){  
            expect(() => { throw Error('Error') }, 'to throw', Error('Error'))      
        })
    })
})