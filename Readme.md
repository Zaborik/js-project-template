# Node JS Template

Template for Node JS projects at AiFi.

# Setup

```bash
brew install yarn
yarn install
```

Once the node JS proto compilation is automated as an npm package, you will not need to do the following:

```bash
git clone git@gitlab.com:aifi-ml/production/proto-contracts.git
npm install -g grpc-tools
mkdir protos
protoc -I proto-contracts --js_out=import_style=commonjs:protos --grpc_out=protos --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` proto-contracts/**/*.proto
```

# Launch

```bash
npm start
```

# Testing

```bash
npm test
```